# Wololo me tender #

### What is this repository for? ###

* This is a Unity project for a mage duel arena game made during Ludum Dare in January 2018.

### How do I get set up? ###

* Download the whole repository and build the game for Windows.

### I want to try this out ###

* Here is a download link to the zip file containing a built .exe. This is built for Windows OS: https://drive.google.com/open?id=1QxkrvgS7JtiBekLlO7pbOW94tvGlCjsa

### Who do I talk to? ###

* Contact mikson60@gmail.com or jens.mikson@gmail.com for any questions regarding this project.