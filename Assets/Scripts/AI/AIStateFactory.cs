﻿
public class AIStateFactory {

	public AI.AIState.Stationary Stationary { get; protected set; }
    public AI.AIState.SearchTarget Searching { get; protected set; }
    public AI.AIState.Moving Moving { get; protected set; }
    public AI.AIState.Attacking Attacking { get; protected set; }

    public AIStateFactory(AI context)
    {
        Stationary = new AI.AIState.Stationary();
        Searching = new AI.AIState.SearchTarget();
        Moving = new AI.AIState.Moving();
        Attacking = new AI.AIState.Attacking();

        Stationary.SetContextVariables(this, context);
        Searching.SetContextVariables(this, context);
        Moving.SetContextVariables(this, context);
        Attacking.SetContextVariables(this, context);
    }
}
