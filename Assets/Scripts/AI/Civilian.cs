﻿using System.Collections.Generic;
using UnityEngine;

public class Civilian : AI {

    [SerializeField] SkinnedMeshRenderer skinnedMeshRenderer;

    protected override Target GetTarget()
    {
        if (alliance == Alliance.Neutral)
        {
            return GetRandomTarget();
        }
        else
        {
            if (mood == Mood.Holy || mood == Mood.Defensive)
            {
                return GameManager.Instance.buildingManager.GetRandomAlliedChurch(new List<Alliance>() { alliance });
            }
            else
            {
                return GameManager.Instance.buildingManager.GetRandomEnemyChurch( alliance );
            }
        }
    }

    public override void SetColor(Color newCol)
    {
        skinnedMeshRenderer.material.color = newCol;
    }
}
