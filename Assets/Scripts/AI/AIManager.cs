﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AIManager : MonoBehaviour {

    [SerializeField] GameObject civilianPrefab;

    Dictionary<AI, Alliance> aiDict = new Dictionary<AI, Alliance>();


    private void OnEnable()
    {
        GameEvent.OnGameWarmup += OnGameWarmup;
        GameEvent.OnGameStart += OnGameStart;
        GameEvent.OnGameEnd += OnGameEnd;

        AIEvent.OnAIConverted += OnAIConverted;
    }
    private void OnDisable()
    {
        GameEvent.OnGameWarmup -= OnGameWarmup;
        GameEvent.OnGameStart -= OnGameStart;
        GameEvent.OnGameEnd -= OnGameEnd;

        AIEvent.OnAIConverted -= OnAIConverted;
    }

    private void OnAIConverted(AI ai)
    {
        if (aiDict.ContainsKey(ai))
        {
            aiDict[ai] = ai.GetAlliance();
        }
        else
        {
            Debug.LogWarning("Unregistered AI present: " + ai.name);
        }
    }

    private void OnGameWarmup(GameSettings settings)
    {
        DestroyAllAI();
    }

    private void OnGameStart(GameSettings settings)
    {
        CreateAI(settings.startingCivilianCount);
    }

    private void OnGameEnd(GameSettings settings, List<Alliance> winners)
    {
        DestroyAllAI();
    }

    public void CreateAI(int amount)
    {
        for (int i=0; i < amount; i++)
        {
            AI newAi = GameManager.Instance.spawnManager.SpawnAI(Alliance.Neutral, civilianPrefab);
            if (newAi != null)
            {
                newAi.SetAlliance(newAi.GetAlliance());
                RegisterAI(newAi);
            }
        }
    }
    public void DestroyAI(AI ai)
    {
        if (aiDict.ContainsKey(ai)) { aiDict.Remove(ai); }
        DestroyImmediate(ai.gameObject);
    }
    public void DestroyAllAI()
    {
        foreach (KeyValuePair<AI, Alliance> kvp in aiDict)
        {
            DestroyImmediate(kvp.Key.gameObject);
        }
        aiDict.Clear();
    }

    

    void RegisterAI(AI ai) { if (!aiDict.ContainsKey(ai)) { aiDict.Add(ai, ai.GetAlliance()); } }
    void DeregisterAI (AI ai) { if (aiDict.ContainsKey(ai)) { aiDict.Remove(ai); } }
}
