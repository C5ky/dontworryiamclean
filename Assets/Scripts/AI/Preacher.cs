﻿using UnityEngine;

[RequireComponent(typeof(Wololo))]
public class Preacher : AI {
	protected override Target GetTarget()
	{
		return GetRandomTarget();
	}
	
	
	public override void SetAlliance(Alliance alliance)
	{
        GetComponent<Player>().alliance = alliance;
		this.alliance = alliance;
	}
}
