﻿using UnityEngine;

public partial class AI
{
    abstract public class AIState
    {

        AI context;
        public AIStateFactory factory;

        virtual public void OnEnter() { }
        virtual public void Update() { }

        public void SetContextVariables(AIStateFactory factory, AI context)
        {
            this.factory = factory;
            this.context = context;
        }

        public class Stationary : AIState
        {
            public override void OnEnter()
            {
                base.OnEnter();
            }
            public override void Update()
            {
                base.Update();
            }
        }

        public class SearchTarget : AIState
        {
            public override void OnEnter()
            {
                base.OnEnter();
                context.currentTarget = null;
            }
            public override void Update()
            {
                base.Update();
                
                Target newTarget = context.GetTarget();

                if (newTarget)
                {
                    context.MoveToTarget(newTarget);
                }
            }
        }

        public class Moving : AIState
        {
            public override void OnEnter()
            {
                base.OnEnter();

                context.agent.SetDestination(context.currentTarget.transform.position);
            }
            public override void Update()
            {
                base.Update();

                if (context.IsTargetInProximity(context.currentTarget))
                {
                    context.currentTarget.Interact(context);
                }
            }
        }

        public class Attacking : AIState
        {
            public override void OnEnter()
            {
                base.OnEnter();
            }
            public override void Update()
            {
                base.Update();
            }
        }
    }
}

