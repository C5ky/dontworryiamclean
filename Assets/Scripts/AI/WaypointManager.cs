﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WaypointManager : MonoBehaviour {

    List<Waypoint> activeWaypoints = new List<Waypoint>();

	void Awake () {
        activeWaypoints = FindObjectsOfType<Waypoint>().ToList();
	}

    public Waypoint GetRandomWaypoint()
    {
        if (activeWaypoints != null && activeWaypoints.Count > 0) { return activeWaypoints[Random.Range(0, activeWaypoints.Count-1)]; }
        else { return null; }
    }
}
