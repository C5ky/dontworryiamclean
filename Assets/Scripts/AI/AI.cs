﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public enum Alliance { Neutral, P1, P2 };
public enum Mood { Holy, Defensive, Offensive }

[RequireComponent(typeof(NavMeshAgent))]
public partial class AI : MonoBehaviour, IAlliance {

    [SerializeField] protected Alliance alliance = Alliance.Neutral;
    [SerializeField] protected Mood mood = Mood.Holy;

    [SerializeField] AudioSource voiceAS;
    [SerializeField] AudioEvent convertAudioEvent;

    [Range(0.1f, 3f)]
    [SerializeField] float proximityRange = 2f;
    NavMeshAgent agent;
    Target currentTarget;

    private AIState _state;
    private AIState State { get { return _state; } set { _state = value; _state.OnEnter(); } }
    private AIStateFactory factory;

    [Range(1, 10)]
    [SerializeField] float immunityMax = 5f;

    Coroutine ImmunityRefreshRoutine;
    bool _isImmune;
    bool Immune { get { return _isImmune; } set { _isImmune = value; if (value) { ImmunityRefreshRoutine = StartCoroutine(RefreshImmunityRoutine()); }  }  }

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();

        factory = new AIStateFactory(this);
        SetState(factory.Searching);
    }

    private void Update()
    {
        State.Update();
        

    }

    protected virtual Target GetTarget()
    {
        if (alliance == Alliance.Neutral)
        {
            return GetRandomTarget();
        }
        else
        {
            return GetChurchTarget();
        }
    }
    protected Target GetRandomTarget()
    {
        currentTarget = GameManager.Instance.waypointManager.GetRandomWaypoint();
        return (currentTarget);
    }

    Target GetChurchTarget()
    {
        return GameManager.Instance.buildingManager.GetRandomEnemyChurch(alliance);
    }

    protected void MoveToTarget(Target target)
    {
        if (target != null && agent != null)
        {
            currentTarget = target;
            SetState(factory.Moving);
        }
    }
    

    protected bool IsTargetInProximity(Target target)
    {
        if (target != null)
        {
            Vector2 xzTransformPosition = new Vector2(transform.position.x, transform.position.z);
            Vector2 xzTargetPosition = new Vector2(target.transform.position.x, target.transform.position.z);

            if (Vector2.Distance(xzTransformPosition, xzTargetPosition) < proximityRange)
            { return true; }
            else
            { return false; }
        }
        else { return true; }
    }

    public void ReachedWaypoint(Waypoint wp)
    {
        SetState(factory.Searching);
    }
    
    public Alliance GetAlliance() { return alliance; }
    public virtual void SetAlliance(Alliance alliance)
    {
        this.alliance = alliance;
    }

    public void SetState(AIState state)
    {
        if (state != null) { State = state; }
    }

    public void PlayerInRange(Alliance alliance)
    {
        Debug.Log("AI(" + name + ").PlayerInRange(" + alliance.ToString() + ").");
        Converted(alliance);
    }

    IEnumerator RefreshImmunityRoutine()
    {
        yield return new WaitForSeconds(immunityMax);
        Immune = false;
    }

    public bool IsImmune() { return Immune; }
    public void Converted(Alliance alliance) {
        if (Immune) { return; }

        Immune = true;

        SetAlliance(alliance);
        SetState(factory.Searching);

        Color newColor = alliance == Alliance.P1 ? Color.magenta : Color.cyan;
        SetColor(newColor);

        convertAudioEvent.Play(voiceAS);

        AIEvent.AIConverted(this);
    }

    public virtual void SetColor(Color newCol)  { }
}