﻿using UnityEngine;

public class Waypoint : Target {

    private void Awake()
    {
        GetComponent<MeshRenderer>().enabled = false;
    }

    public override void Interact(AI ai)
    {
        ai.ReachedWaypoint(this);
    }
}
