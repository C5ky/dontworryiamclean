﻿using UnityEngine;

public class Target : MonoBehaviour {

    public virtual void Interact(AI ai) { }
}
