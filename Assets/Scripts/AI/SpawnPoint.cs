﻿using UnityEngine;

public class SpawnPoint : MonoBehaviour {

    public Alliance alliance;

    private void Awake()
    {
        GetComponent<MeshRenderer>().enabled = false;
    }
}
