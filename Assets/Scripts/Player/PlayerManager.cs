﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    [SerializeField] GameObject playerPrefab;

    Dictionary<Player, string> players = new Dictionary<Player, string>();

    int totalAlliances = 0;
    List<Alliance> readyAlliances = new List<Alliance>();

    private void OnEnable()
    {
        GameEvent.OnGameWarmup += OnGameWarmup;

        PlayerEvent.OnPlayerReady += OnPlayerReady;
    }
    private void OnDisable()
    {
        GameEvent.OnGameWarmup -= OnGameWarmup;

        PlayerEvent.OnPlayerReady -= OnPlayerReady;
    }

    private void OnGameWarmup(GameSettings settings)
    {
        foreach (KeyValuePair<Player, string> kvp in players)
        {
            kvp.Key.transform.position = GameManager.Instance.spawnManager.GetRandomSpawnPointByAlliance(kvp.Key.GetAlliance()).transform.position;
        }

        readyAlliances.Clear();

        if (players.Count == 0)
        {
            foreach (Alliance alliance in settings.allStartingAlliances)
            {
                if (alliance == Alliance.Neutral) { continue; }
                CreatePlayer(alliance);
            }
        }
    }

    private void OnPlayerReady(Alliance alliance, bool isReady)
    {
        if (isReady)
        {
            if (!readyAlliances.Contains(alliance)) { readyAlliances.Add(alliance); CheckReadyStatus(); }
        }
        else
        {
            if (readyAlliances.Contains(alliance)) { readyAlliances.Remove(alliance); }
            if (readyAlliances.Count == 1)
            {
                PlayerEvent.CancelAllPlayersReady();
            }
        }
    }

    void CheckReadyStatus()
    {
        if (readyAlliances.Count >= totalAlliances) // TODO: change this
        {
            PlayerEvent.AllPlayersReady();
        }
    }

    void CreatePlayer(Alliance alliance)
    {
        Player player = GameManager.Instance.spawnManager.SpawnPlayer(alliance, playerPrefab);
        if (player != null)
        {
            RegisterPlayer(player);
            if (player.alliance != Alliance.Neutral) { totalAlliances++; }
        }
    }

    void RegisterPlayer(Player player)
    {
        if (player != null && !players.ContainsKey(player)) { players.Add(player, player.name); }
    }
}
