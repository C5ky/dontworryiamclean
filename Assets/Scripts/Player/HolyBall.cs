﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolyBall : MonoBehaviour
{
    public AnimationCurve normalizedFlightCurve;

    private bool enroute;
    private Transform peasant;
    private float totalFlightTime;
    private float accumulatedTime;
    private Vector3 startPosition;
    private Alliance oneTrueGod;

    [SerializeField] AudioSource fireAS;
    [SerializeField] AudioEvent wololoAE;

	// Update is called once per frame
	void Update () {
		if(!enroute)
            return;

	    accumulatedTime += Time.deltaTime;
        var curvedTime = normalizedFlightCurve.Evaluate(accumulatedTime / totalFlightTime);
        if (peasant != null)
        {
            transform.position = Vector3.Lerp(startPosition, peasant.position, curvedTime);
        }
	    

	    if (curvedTime >= 1f)
	    {
            if (peasant != null && peasant.GetComponent<IAlliance>() != null)
            {
                peasant.GetComponent<IAlliance>().Converted(oneTrueGod);
            }
	        this.enabled = false;
	        GetComponentInChildren<ParticleSystem>().Stop();
            Invoke("CleanUp" ,3f);
	    }
	}

    void CleanUp()
    {
        Destroy(gameObject);
    }

    public void FireAt(IAlliance ai, float flightTime, Alliance alliance)
    {
        if(ai == null)
            return;

        wololoAE.Play(fireAS);
        peasant = ai.GetComponent<Transform>();
        oneTrueGod = alliance;
        totalFlightTime = flightTime;
        startPosition = transform.position;
        accumulatedTime = 0;
        enroute = true;
    }
}
