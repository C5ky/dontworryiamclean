﻿using System;
using UnityEngine;

public class StartController : MonoBehaviour
{
	[SerializeField] private Alliance _alliance;

    [SerializeField] BoxCollider coll;
    [SerializeField] MeshRenderer mr;

    [SerializeField] AudioSource audioS;
    [SerializeField] AudioEvent aEvent;

    private void OnEnable()
    {
        GameEvent.OnGameWarmup += OnGameWarmup;
        GameEvent.OnGameStart += OnGameStart;
    }
    private void OnDisable()
    {
        GameEvent.OnGameWarmup -= OnGameWarmup;
        GameEvent.OnGameStart -= OnGameStart;
    }

    private void OnWarmupCountdownChange(int currentCountdown)
    {
        throw new NotImplementedException();
    }

    private void OnGameWarmup(GameSettings settings)
    {
        coll.enabled = true;
        mr.enabled = true;
    }

    private void OnGameStart(GameSettings settings)
    {
        coll.enabled = false;
        mr.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
	{
		var temp = other.GetComponent<Player>();
		
		if (temp != null && temp.alliance == _alliance)
		{
            PlayerEvent.PlayerReady(temp.alliance, true);
            aEvent.Play(audioS);
		}
	}
	
	private void OnTriggerExit(Collider other)
	{
		var temp = other.GetComponent<Player>();
		
		if (temp != null && temp.alliance == _alliance)
		{
            Debug.Log("aa");
            PlayerEvent.PlayerReady(temp.alliance, false);
        }
	}
}
