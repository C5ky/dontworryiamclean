﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class PlayerMove : MonoBehaviour
{    
	[SerializeField] AudioSource punchSource;
	[SerializeField] private AudioEvent punchAudioEvent;
	
	NavMeshAgent _agent;
    Animator anim;

	private KeyCode _upKeyCode;
	private KeyCode _downKeyCode;
	private KeyCode _leftKeyCode;
	private KeyCode _rightKeyCode;
	private KeyCode _setToOld;
	private KeyCode _punch;
	
	private string _horizontal;
	private string _vertical;
    private float speed;
		
	private bool _movingUp;
	private bool _movingDown;
	private bool _movingLeft;
	private bool _movingRight;

	private bool _inputDisabled;
	
	private bool oldControls;
	
	private const float OneDivSqrt2 = 0.70710678118654752440084436210485f;

	private float punchcountdown;
	private float punchcooldown;
	
	private Vector2 _leftright;
	private Vector2 _updown;

    public Player player;
    [SerializeField] GameObject cryObject;
	
	private void Awake()
	{
		_agent = GetComponent<NavMeshAgent>();
	}

	
	private void Start()
	{
		_inputDisabled = false;
		oldControls = false;
        anim = GetComponent<Animator>();

        var temp = GetComponent<Player>();
		if (temp.alliance == Alliance.P1)
		{
			_upKeyCode = KeyCode.UpArrow;
			_downKeyCode = KeyCode.DownArrow;
			_leftKeyCode = KeyCode.LeftArrow;
			_rightKeyCode = KeyCode.RightArrow;

			_setToOld = KeyCode.F12;
			_horizontal = "Horizontal";
			_vertical = "Vertical";
			_punch = KeyCode.RightControl;
		}
		if (temp.alliance == Alliance.P2)
		{
			_setToOld = KeyCode.F1;
			_upKeyCode = KeyCode.W;
			_downKeyCode = KeyCode.S;
			_leftKeyCode = KeyCode.A;
			_rightKeyCode = KeyCode.D;
			_horizontal = "Horizontal2";
			_vertical = "Vertical2";
			_punch = KeyCode.Q;
		}
	}


    private void Update()
    {
	    if (_inputDisabled) return;

	    if (punchcooldown > 0) punchcooldown -= Time.deltaTime;

	    if (punchcountdown > 0) return;
	    
	
	    
	    if (Input.GetKeyDown(_setToOld)) oldControls = !oldControls;


	    if (oldControls)
	    {
		    var x = Input.GetAxis(_horizontal) * Time.deltaTime * 150;
		    var z = Input.GetAxis(_vertical) * Time.deltaTime * 15;
		    print(Input.GetAxis(_vertical));
		    transform.Rotate(0, x, 0);
		    transform.Translate(0,0,z);
		    anim.SetFloat("speed", Input.GetAxis(_vertical));
		    return;
	    }

	    Up:
	    if (Input.GetKey(_upKeyCode))
	    {
		    if (_movingUp) goto Down;
			
		    _movingUp = true;
		    _updown	= Vector2.up;
	    }
	    else
	    {
		    _movingUp = false;
		    _updown = (_movingDown) ? Vector2.down : Vector2.zero;
	    }
		
	    Down:
	    if (Input.GetKey(_downKeyCode))
	    {
		    if (_movingDown) goto Left;
			
		    _movingDown = true;
		    _updown	= Vector2.down;
	    }
	    else
	    {
		    _movingDown = false;
		    _updown = (_movingUp) ? Vector2.up : Vector2.zero;
	    }
		
	    Left:
	    if (Input.GetKey(_leftKeyCode))
	    {
		    if (_movingLeft) goto Right;
			
		    _movingLeft = true;
		    _leftright	= Vector2.left;
	    }
	    else
	    {
		    _movingLeft = false;
		    _leftright = (_movingRight) ? Vector2.right : Vector2.zero;
	    }
		
	    Right:
	    if (Input.GetKey(_rightKeyCode))
	    {
		    if (_movingRight) goto Move;

		    _movingRight = true;
		    _leftright = Vector2.right;
	    }
	    else
	    {
		    _movingRight = false;
		    _leftright = (_movingLeft) ? Vector2.left : Vector2.zero;
	    }
		
	    Move:
  

        //speed = Mathf.Clamp(speed, -1f, 1f);
	    
	    var temp = GetMovementVector(_leftright, _updown);

	    if (Mathf.Approximately(temp.magnitude, 0))
	    {
		    anim.SetFloat("speed", 0);
		    return;
	    } 
	    
	    transform.rotation = Quaternion.LookRotation(temp);
	    //print(temp.magnitude);

	    transform.Translate(0, 0, Time.deltaTime * 15);
	    //transform.Translate((temp * Time.deltaTime * 15));
        anim.SetFloat("speed", 1);
        // print(Input.GetAxis(_vertical));
    }

    private void OnEnable()
    {
        GameEvent.OnGameWarmup += OnGameWarmup;
        GameEvent.OnGameEnd += OnGameEnd;
    }
    private void OnDisable()
    {
        GameEvent.OnGameWarmup -= OnGameWarmup;
        GameEvent.OnGameEnd -= OnGameEnd;
    }

    private void OnGameWarmup(GameSettings settings)
    {
        cryObject.SetActive(false);
        anim.SetBool("dance", false);
        anim.SetBool("cry", false);
        InputDisabled = false;
    }

    private void OnGameEnd(GameSettings settings, List<Alliance> winners)
    {
        InputDisabled = true;
        if (winners == null)
        {
            winners = GameManager.Instance.scoreManager.GetWinners();
            if (winners.Contains(player.alliance))
            {
                cryObject.SetActive(false);
                anim.SetBool("dance", true);
                anim.SetBool("cry", false);
            }
            else
            {
                cryObject.SetActive(true);
                anim.SetBool("cry", true);
                anim.SetBool("dance", false);
            }
        }
        else
        {
            if (winners.Contains(player.alliance))
            {
                cryObject.SetActive(false);
                anim.SetBool("dance", true);
                anim.SetBool("cry", false);
            }
            else
            {
                cryObject.SetActive(true);
                anim.SetBool("cry", true);
                anim.SetBool("dance", false);
            }
        }
    }

    /*private void Update ()
	{
		var x = Input.GetAxis(_horizontal);
		var z = Input.GetAxis(_vertical);
		var targetPos = transform.position;
		targetPos.z += z;
		targetPos.x += x;

		_agent.SetDestination(targetPos);
	}*/


    public bool InputDisabled
	{
		set
		{
			anim.SetFloat("speed", 0);
			_inputDisabled = value;
		}
	}
	
	private void OnTriggerStay(Collider other) {
		if (_inputDisabled) return;
		if (other.gameObject == gameObject) return;
		if (!other.GetComponent<Player>()) return;
		if (!Input.GetKey(_punch)) return;
		if (punchcooldown > 0) return;

		// calculate force vector
		var force = transform.position - other.transform.position;
		// normalize force vector to get direction only and trim magnitude
		force.Normalize();
		other.GetComponent<PlayerMove>().ResetPunchCounter(force);
		
		punchcooldown = 2;
	}
	
	
	private static Vector3 GetMovementVector(Vector2 a, Vector2 b)
	{
		var temp = a + b;

		if (Mathf.Approximately(temp.magnitude, 0)) return Vector3.zero;
		
		return (Mathf.Approximately(temp.magnitude, 1)) ? 
			new Vector3(temp.x, 0, temp.y) : new Vector3(temp.x * OneDivSqrt2, 0, temp.y * OneDivSqrt2);
	}

	public void ResetPunchCounter(Vector3 force)
	{
		punchcountdown = 0.5f;
		StartCoroutine(PunchC(force));
	}

	private IEnumerator PunchC(Vector3 force)
	{
		GetComponent<Rigidbody>().isKinematic = false;
		punchAudioEvent.Play(punchSource);
		while (punchcountdown > 0)
		{
			punchcountdown -= Time.deltaTime;
			GetComponent<Rigidbody>().AddForce(-force * 10000 * punchcountdown);
			//transform.Translate(force * Time.deltaTime * 20);
			yield return null;
		}
		GetComponent<Rigidbody>().isKinematic = true;
	}
}
