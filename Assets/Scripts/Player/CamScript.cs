﻿using UnityEngine;

public class CamScript : MonoBehaviour {
    private int min = 25;
    private int max = 55;

    private int dmin = 20;
    private int dmax = 100;

    public float MidX;
    public float MidY;
    public float MidZ;
    public Transform target1;
    public Transform target2;
    public Vector3 Midpoint;
    //public Vector3 distance;

    private Camera camera;
    // Use this for initialization
    void Start ()
  
    {
        //print(Vector2.Distance(new Vector2(67, 32), new Vector2(-58, -38)));
        //print(Vector2.Distance(new Vector2(26, 5), new Vector2(-8, -18)));
        //camera = GetComponent<Camera>();
        var temp = FindObjectsOfType<Player>();

	    target1 = temp[0].gameObject.transform;
	    target2= temp[1].gameObject.transform;
    }
	
	// Update is called once per frame
	void Update () {
        MidX = (target2.position.x + target1.position.x) / 2;
        MidY = (target2.position.y + target1.position.y) / 2;
        MidZ = (target2.position.z + target1.position.z) / 2;

        var distance = Mathf.Clamp( Vector3.Distance(target2.position, target1.position), dmin, dmax);

        var a = (distance - dmin) / (dmax - dmin);

        var y = (max - min) * a + min;

        Camera.main.fieldOfView = y;

        Midpoint = new Vector3(MidX, transform.position.y, MidZ -43);
		transform.position = Midpoint;
	}
}
