﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IAlliance{

    [SerializeField] List<SkinnedMeshRenderer> colorComponents = new List<SkinnedMeshRenderer>();
    [SerializeField] List<MeshRenderer> colorComponentsMr = new List<MeshRenderer>();

    public Wololo wololo;
    public Alliance alliance;

    private void Start()
    {
        Color newColor = alliance == Alliance.P1 ? Color.magenta : Color.cyan;
        SetColor(newColor);
    }

    public void Converted(Alliance alliance)
    {
        throw new System.NotImplementedException();
    }

    public Alliance GetAlliance()
    {
        return alliance;
    }

    public void SetColor(Color newCol)
    {
        foreach (SkinnedMeshRenderer smr in colorComponents)
        {
            if (smr != null)
            {
                Material mat = smr.material;
                mat.color = newCol;
            }
        }
        foreach (MeshRenderer mr in colorComponentsMr)
        {
            if (mr != null)
            {
                Material mat = mr.material;
                mat.color = newCol;
            }
        }
    }

    public bool IsImmune()
    {
        return true;
    }
}
