﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolyBallsFlingMachine : MonoBehaviour
{

    public float RotationsPerSecond = 1;

    [Header("Gameobject refs")]
    public Transform center;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		center.Rotate(Vector3.up, RotationsPerSecond *  360 * Time.deltaTime);
	}
    
}
