using System.Collections.Generic;
using UnityEngine;

public class Wololo : MonoBehaviour
{
    //Cousin stuff
    public List<Wololo> CousinList;

    //Ball stuff
    public IAlliance LastVictim { get; private set; }
    public float reloadDuration;
    public float ballFlightTime = 1f;
    private float fireTimestamp;
    private HolyBall loadedBall;

    //Other stuff
	private Alliance alliance;
	private List<IAlliance> aiList;

    [SerializeField] HolyBall ballPrefabP1;
    [SerializeField] HolyBall ballPrefabP2;

    private void Start () {
		aiList = new List<IAlliance>();
	    alliance = GetComponentInParent<Player>().alliance;
	    fireTimestamp = -reloadDuration;
	}

    private void OnEnable()
    {
        GameEvent.OnGameEnd += OnGameEnd;
    }
    private void OnDisable()
    {
        GameEvent.OnGameEnd -= OnGameEnd;
    }

    private void OnGameEnd(GameSettings settings, List<Alliance> winners)
    {
        aiList.Clear();
    }

    private void Update ()
	{
		if (alliance == Alliance.Neutral) return;

	    if (loadedBall == null)
	    {
	        if (Time.time > fireTimestamp + reloadDuration)
	        {
                HolyBall ballPrefab = ballPrefab = alliance == Alliance.P1 ? ballPrefabP1 : ballPrefabP2;
                loadedBall = Instantiate(ballPrefab, transform);
	            LastVictim = null;
	        }
            return;
	    }

        for (var i = aiList.Count - 1; i >= 0; i--)
        {
            /*
            Debug.Log(aiList);
            Debug.Log(aiList[i]);
            Debug.Log(aiList[i].GetComponent<Preacher>());
            Debug.Log(GetComponent<Preacher>());
            if (aiList == null || aiList[i] == null || (aiList[i].GetComponent<Preacher>() && GetComponent<Preacher>())) return;
            */
            if (aiList == null || aiList[i] == null || aiList[i].GetComponent<IAlliance>() == null) { return; }
            
            var peasant = aiList[i].GetComponent<IAlliance>();
            
            if (peasant?.GetAlliance() != alliance 
                && !peasant.IsImmune() && !IsTargetUnderFire(peasant))
		    {
                if (loadedBall != null)
                {
                    loadedBall.FireAt(peasant, ballFlightTime, alliance);
                }
		        fireTimestamp = Time.time;
		        loadedBall = null;
		        LastVictim = peasant;
		    }
		}
	}

    private bool IsTargetUnderFire(IAlliance asd)
    {
        foreach (var cousin in CousinList)
        {
            if (cousin.LastVictim == asd as Object)
                return true;
        }
        return false;
    }

	void OnTriggerEnter(Collider other)
	{
		var temp = other.GetComponent<IAlliance>();
        

        if (temp != null)
        {
            aiList.Add(temp);
        }
		
	}

	void OnTriggerExit(Collider other)
	{
		var temp = other.GetComponent<IAlliance>();

		if (temp != null) aiList.Remove(temp);
	}
}
