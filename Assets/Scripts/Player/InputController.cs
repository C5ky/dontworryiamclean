﻿using UnityEngine;
using UnityEngine.AI;

public class InputController : MonoBehaviour
{
	[SerializeField] private KeyCode _upKeyCode = KeyCode.UpArrow;
	[SerializeField] private KeyCode _downKeyCode = KeyCode.DownArrow;
	[SerializeField] private KeyCode _leftKeyCode = KeyCode.LeftArrow;
	[SerializeField] private KeyCode _rightKeyCode = KeyCode.RightArrow;
	[SerializeField] private float _speed = 5;

	[SerializeField] private GameObject _target;
	
	private Vector2 _leftright;
	private Vector2 _updown;

	private const float OneDivSqrt2 = 0.70710678118654752440084436210485f;
	
	private bool _movingUp;
	private bool _movingDown;
	private bool _movingLeft;
	private bool _movingRight;

	NavMeshAgent agent;

	private void Awake()
	{
		agent = GetComponent<NavMeshAgent>();
	}

	
	private void Update ()
	{
		Up:
		if (Input.GetKey(_upKeyCode))
		{
			if (_movingUp) goto Down;
			
			_movingUp = true;
			_updown	= Vector2.up;
		}
		else
		{
			_movingUp = false;
			_updown = (_movingDown) ? Vector2.down : Vector2.zero;
		}
		
		Down:
		if (Input.GetKey(_downKeyCode))
		{
			if (_movingDown) goto Left;
			
			_movingDown = true;
			_updown	= Vector2.down;
		}
		else
		{
			_movingDown = false;
			_updown = (_movingUp) ? Vector2.up : Vector2.zero;
		}
		
		Left:
		if (Input.GetKey(_leftKeyCode))
		{
			if (_movingLeft) goto Right;
			
			_movingLeft = true;
			_leftright	= Vector2.left;
		}
		else
		{
			_movingLeft = false;
			_leftright = (_movingRight) ? Vector2.right : Vector2.zero;
		}
		
		Right:
		if (Input.GetKey(_rightKeyCode))
		{
			if (_movingRight) goto Move;

			_movingRight = true;
			_leftright = Vector2.right;
		}
		else
		{
			_movingRight = false;
			_leftright = (_movingLeft) ? Vector2.left : Vector2.zero;
		}
		
	Move:Debug.Log("");
//		_target.transform.position = transform.position + (GetMovementVector(_leftright, _updown) * Time.deltaTime * _speed);
		
//		agent.SetDestination(_target.transform.position);
	}

	
	private static Vector3 GetMovementVector(Vector2 a, Vector2 b)
	{
		var temp = a + b;

		if (Mathf.Approximately(temp.magnitude, 0)) return Vector3.zero;
		
		return (Mathf.Approximately(temp.magnitude, 1)) ? 
			new Vector3(temp.x, 0, temp.y) : new Vector3(temp.x * OneDivSqrt2, 0, temp.y * OneDivSqrt2);
	}
}
