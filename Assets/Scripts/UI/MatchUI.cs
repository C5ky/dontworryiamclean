﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchUI : MonoBehaviour {

    [Serializable]
    struct AlliancePrayer
    {
        public Alliance alliance;
        public Text prayerText;
    }

    [SerializeField] List<AlliancePrayer> alliancePrayerUI = new List<AlliancePrayer>();
    [SerializeField] Text titleText;

    int winnersCount = 0;
    string winner = "";

    private void OnEnable()
    {
        GameEvent.OnScoreChange += OnScoreChange;
        GameEvent.OnGameWarmup += OnGameWarmup;
        GameEvent.OnGameEnd += OnGameEnd;

        GameEvent.OnEndCountdownChange += OnEndCountdownChange;
        GameEvent.OnPlayCountdownChange += OnPlayCountdownChange;
        GameEvent.OnWarmupCountdownChange += OnWarmupCountdownChange;
        PlayerEvent.OnCancelAllPlayersReady += OnCancelAllPlayersReady;
    }
    private void OnDisable()
    {
        GameEvent.OnScoreChange -= OnScoreChange;
        GameEvent.OnGameWarmup -= OnGameWarmup;
        GameEvent.OnGameEnd -= OnGameEnd;

        GameEvent.OnEndCountdownChange -= OnEndCountdownChange;
        GameEvent.OnPlayCountdownChange -= OnPlayCountdownChange;
        GameEvent.OnWarmupCountdownChange -= OnWarmupCountdownChange;
        PlayerEvent.OnCancelAllPlayersReady -= OnCancelAllPlayersReady;
    }

    private void OnGameWarmup(GameSettings settings)
    {
        winner = "";
        winnersCount = 0;

        if (titleText != null)
        {
            titleText.text = "Warmup (stand on platforms)";
        }
    }

    private void OnGameEnd(GameSettings settings, List<Alliance> winners)
    {
        if (titleText != null)
        {
            if (winners == null)
            {
                winners = GameManager.Instance.scoreManager.GetWinners();
                if (winners.Count == 1)
                {
                    winner = winners[0].ToString();
                    winnersCount = 1;
                    titleText.text = "Winner " + winner;
                }
                else if (winners.Count > 1)
                {
                    winner = winners[0].ToString();
                    winnersCount = winners.Count;
                    titleText.text = "Tie! ";
                }
            }
            else
            {
                winner = winners[0].ToString();
                winnersCount = 1;
                titleText.text = "Winner " + winner;
            }
        }
    }

    private void OnScoreChange(Alliance alliance, int amount)
    {
        Text alliancePrayerText = GetAlliancePrayerText(alliance);
        if (alliancePrayerText == null)
        {
            Debug.LogWarning("Prayer UI missing for " + alliance.ToString());
        }
        else
        {
            alliancePrayerText.text = "Prayers: " + amount.ToString();
        }
    }

    private void OnEndCountdownChange(int currentCountdown)
    {
        if (titleText != null)
        {
            if (winnersCount == 1)
            {
                titleText.text = titleText.text = "Winner " + winner + " ..." + currentCountdown.ToString();
            }
            else if (winnersCount > 1)
            {
                titleText.text = titleText.text = "Tie..." + currentCountdown.ToString();
            }
        }
    }

    private void OnPlayCountdownChange(int currentCountdown)
    {
        if (titleText != null)
        {
            titleText.text = "Time: " + currentCountdown.ToString();
        }
    }

    private void OnWarmupCountdownChange(int currentCountdown)
    {
        if (titleText != null)
        {
            titleText.text = "Starting..." + currentCountdown.ToString();
        }
    }

    private void OnCancelAllPlayersReady()
    {
        if (titleText != null)
        {
            titleText.text = "Warmup (stand on platforms)";
        }
    }

    Text GetAlliancePrayerText(Alliance alliance)
    {
        Text allianceText = null;

        foreach (AlliancePrayer ap in alliancePrayerUI)
        {
            if (ap.alliance == alliance && ap.prayerText != null) { allianceText = ap.prayerText; }
        }

        return allianceText;
    }
}
