﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class OptionsPanel : MonoBehaviour {

    public AudioMixer mixer;
    public GameObject mainPanel;
    public AnimationCurve audioLevel;

    public void Back()
    {
        gameObject.SetActive(false);
        mainPanel.SetActive(true);
    }

    public void SetSfx(float level)
    {
        float eval = audioLevel.Evaluate(level);
        float a = (1 - eval) * -80;
        mixer.SetFloat("SFX", eval);
    }

    public void SetMusic(float level)
    {
        float eval = audioLevel.Evaluate(level);
        float a = (1 - eval) * -80;
        mixer.SetFloat("Music", a);
    }
}
