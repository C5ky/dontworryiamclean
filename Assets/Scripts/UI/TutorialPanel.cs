﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialPanel : MonoBehaviour
{

    public Image UIImageRef;
    public Sprite Sprite;

    public GameObject nextButton;
    public GameObject closeButton;
    public TutorialPanel next;

    private void OnEnable()
    {
        UIImageRef.sprite = Sprite;
        nextButton.SetActive(next != null);
        closeButton.SetActive(next == null);
    }

    public void Next()
    {
        gameObject.SetActive(false);
        next.gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

}
