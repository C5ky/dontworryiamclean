﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {


    [SerializeField] AudioSource announcementAS;
    [SerializeField] AudioEvent gameEndAE;

    private void OnEnable()
    {
        GameEvent.OnGameEnd += OnGameEnd;
        GameEvent.OnWarmupCountdownChange += OnWarmupCountdownChange;
        GameEvent.OnPlayCountdownChange += OnPlayCountdownChange;
    }
    private void OnDisable()
    {
        GameEvent.OnGameEnd -= OnGameEnd;
        GameEvent.OnWarmupCountdownChange -= OnWarmupCountdownChange;
        GameEvent.OnPlayCountdownChange -= OnPlayCountdownChange;
    }

    private void OnGameEnd(GameSettings settings, List<Alliance> winners)
    {
        if (gameEndAE != null && announcementAS != null) { gameEndAE.Play(announcementAS); }
    }
    private void OnWarmupCountdownChange(int currentCountdown)
    {
        if (gameEndAE != null && announcementAS != null) { gameEndAE.Play(announcementAS); }
    }
    private void OnPlayCountdownChange(int currentCountdown)
    {
        if (currentCountdown <= 10)
        {
            if (gameEndAE != null && announcementAS != null) { gameEndAE.Play(announcementAS); }
        }
    }
}
