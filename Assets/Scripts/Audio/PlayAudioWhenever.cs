﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudioWhenever : MonoBehaviour
{

    public float Whenever;
    public AudioEvent AudioEvent;

    private float stamp = 0f;

	// Update is called once per frame
	void Update () {
		if(Time.time < stamp)
            return;

	    stamp = Time.time + Whenever;
	    var source = GetComponent<AudioSource>();
        AudioEvent.Play(source);

	}

}
