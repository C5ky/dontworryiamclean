﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[CreateAssetMenu]
public class AudioEvent : ScriptableObject {

    public AudioClip[] Clips;
    public float[] Weights;

    [Range(0,2)]
    public float VolumeMin = 1f;
    [Range(0, 2)]
    public float VolumeMax = 1f;
    [Range(0, 2)]
    public float PitchMin = 1f;
    [Range(0, 2)]
    public float PitchMax = 1f;

    private float totalWeight;

    public float cooldown = 1f;
    private float timeStamp;

    private void OnEnable()
    {
        if(Clips == null || Weights == null)
            return;

        timeStamp = -cooldown;
        float totalWeight = 0;
        for (int i = 0; i < Clips.Length; ++i)
            totalWeight += Weights[i];
    }


    public void Play(AudioSource source)
    {
        if (Clips.Length == 0) return;
        if (Time.time < timeStamp+cooldown) { return; }

        timeStamp = Time.time;

        source.clip = Clips[Random.Range(0, Clips.Length)];
        source.volume = Random.Range(VolumeMin, VolumeMax);
        source.pitch = Random.Range(PitchMin, PitchMax);
        source.Play();
    }
}
