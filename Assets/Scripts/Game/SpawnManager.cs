﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    Dictionary<Alliance, List<SpawnPoint>> spawnPoints = new Dictionary<Alliance, List<SpawnPoint>>();

    private void Awake()
    {
        foreach (SpawnPoint spawnPoint in FindObjectsOfType<SpawnPoint>())
        {
            if (spawnPoint == null) { continue; }
            if (spawnPoints.ContainsKey(spawnPoint.alliance))
            {
                if (spawnPoints[spawnPoint.alliance] != null && !spawnPoints[spawnPoint.alliance].Contains(spawnPoint))
                {
                    spawnPoints[spawnPoint.alliance].Add(spawnPoint);
                }
            }
            else { spawnPoints.Add(spawnPoint.alliance, new List<SpawnPoint>() { spawnPoint }); }
        }
    }

    public AI SpawnAI(Alliance alliance, GameObject aiPrefab)
    {
        SpawnPoint spawnPoint = GetRandomSpawnPointByAlliance(alliance);
        if (spawnPoint != null && aiPrefab != null)
        {
            AI ai = Instantiate(aiPrefab, spawnPoint.transform.position, spawnPoint.transform.rotation).GetComponent<AI>();
            if (ai != null)
            {
                return ai;
            }
        }
        return null;
    }

    public Player SpawnPlayer(Alliance alliance, GameObject playerPrefab)
    {
        SpawnPoint spawnPoint = GetRandomSpawnPointByAlliance(alliance);
        if (spawnPoint != null && playerPrefab != null)
        {
            Player player = Instantiate(playerPrefab, spawnPoint.transform.position, spawnPoint.transform.rotation).GetComponent<Player>();
            if (player != null)
            {
                player.alliance = alliance;
                return player;
            }
        }
        return null;
    }

    public SpawnPoint GetRandomSpawnPointByAlliance(Alliance alliance)
    {
        if (spawnPoints != null && spawnPoints.ContainsKey(alliance) && spawnPoints[alliance] != null && spawnPoints[alliance].Count > 0)
        {
            int index = Random.Range(0, spawnPoints[alliance].Count);
            return spawnPoints[alliance][index];
        }
        else { return null; }
    }
}
