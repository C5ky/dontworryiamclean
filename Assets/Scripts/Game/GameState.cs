﻿
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager
{
    abstract public class GameState
    {
        GameManager context;
        public GameStateFactory factory;

        virtual public void OnEnter() { }
        virtual public void Update() { }

        public void SetContextVariables(GameStateFactory factory, GameManager context)
        {
            this.factory = factory;
            this.context = context;
        }

        public class Warmup : GameState
        {
            public override void OnEnter()
            {
                base.OnEnter();
                context.StopAllCoroutines();
                GameEvent.GameWarmup(context.settings);
            }
            public override void Update()
            {
                base.Update();
            }
        }

        public class Play : GameState
        {
            List<Alliance> winners = new List<Alliance>();

            public override void OnEnter()
            {
                base.OnEnter();

                winners = new List<Alliance>();
                context.StopAllCoroutines();
                context.StartCoroutine(context.PlayRoutine());
                GameEvent.GameStart(context.settings);
            }
            public override void Update()
            {
                base.Update();
                winners = context.scoreManager.CheckScore();
                if (winners.Count > 0)
                {
                    GameEvent.GameEnd(context.settings, winners);
                }
            }
        }

        public class End : GameState
        {
            public override void OnEnter()
            {
                base.OnEnter();
                context.StopAllCoroutines();
                context.StartCoroutine(context.EndRoutine());
            }
            public override void Update()
            {
                base.Update();
            }
        }
    }
}
