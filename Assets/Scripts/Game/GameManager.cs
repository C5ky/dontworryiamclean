﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    public static GameManager Instance;

    public WaypointManager waypointManager;
    public AIManager aiManager;
    public PlayerManager playerManager;
    public BuildingManager buildingManager;
    public SpawnManager spawnManager;
    public ScoreManager scoreManager;

    private GameState _state;
    private GameState State { get { return _state; } set { _state = value; _state.OnEnter(); } }
    private GameStateFactory factory;

    [SerializeField] GameSettings settings = new GameSettings();

    void Awake()
    {
        if (!Instance) { Instance = this; }
        else if (Instance && Instance != this) { Destroy(this); }

        factory = new GameStateFactory(this);
        SetState(factory.Warmup);
    }

    private void OnEnable()
    {
        GameEvent.OnGameEnd += OnGameEnd;

        PlayerEvent.OnAllPlayersReady += OnAllPlayersReady;
        PlayerEvent.OnCancelAllPlayersReady += OnCancelAllPlayersReady;
    }
    private void OnDisable()
    {
        GameEvent.OnGameEnd -= OnGameEnd;

        PlayerEvent.OnAllPlayersReady -= OnAllPlayersReady;
        PlayerEvent.OnCancelAllPlayersReady -= OnCancelAllPlayersReady;
    }

    private void Update()
    {
        State.Update();
    }

    private void OnGameEnd(GameSettings settings, List<Alliance> winners)
    {
        SetState(factory.End);
    }

    private void OnCancelAllPlayersReady()
    {
        if (State == factory.Warmup)
        {
            StopAllCoroutines();
        }
    }
    private void OnAllPlayersReady()
    {
        if (State == factory.Warmup)
        {
            StopAllCoroutines();
            StartCoroutine(WarmupRoutine());
        }    
    }

    IEnumerator WarmupRoutine()
    {
        int counter = 3;

        while (counter > 0)
        {
            GameEvent.WarmupCountdownChange(counter);
            Debug.Log("Starting..." + counter.ToString());
            yield return new WaitForSeconds(1f);
            counter--;
        }

        SetState(factory.Play);
    }
    protected IEnumerator PlayRoutine()
    {
        int counter = settings.roundTime;

        while (counter > 0)
        {
            GameEvent.PlayCountdownChange(counter);
            yield return new WaitForSeconds(1f);
            counter--;
        }

        GameEvent.GameEnd(settings, null);
    }
    protected IEnumerator EndRoutine()
    {
        int counter = 10;

        while (counter > 0)
        {
            GameEvent.EndCountdownChange(counter);
            Debug.Log("Ending..." + counter.ToString());
            // TODO Update UI
            yield return new WaitForSeconds(1f);
            counter--;
        }

        SetState(factory.Warmup);
    }

    void SetState(GameState state)
    {
        if (state != null) { State = state; }
    }

    public GameSettings GetGameSettings()
    {
        return settings;
    }
}

[System.Serializable]
public struct GameSettings
{
    public int roundTime;
    public int maxScore;
    public int startingCivilianCount;
    public Alliance[] allStartingAlliances;

    public GameSettings(int roundTime, int maxScore, int startingCivCount, Alliance[] startingAlliances)
    {
        this.roundTime = roundTime;
        this.maxScore = maxScore;
        startingCivilianCount = startingCivCount;
        allStartingAlliances = startingAlliances;
    }
}