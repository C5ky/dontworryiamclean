﻿using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    Dictionary<Alliance, int> allianceScores = new Dictionary<Alliance, int>();

    private void OnEnable()
    {
        GameEvent.OnGameWarmup += OnGameWarmup;
    }
    private void OnDisable()
    {
        GameEvent.OnGameWarmup -= OnGameWarmup;
    }

    private void OnGameWarmup(GameSettings settings)
    {
        ResetScore();
    }

    public void AddScore(Alliance alliance, int value)
    {
        if (allianceScores.ContainsKey(alliance))
        {
            allianceScores[alliance] += value;
        }
        else
        {
            allianceScores.Add(alliance, value);
        }

        GameEvent.ScoreChange(alliance, allianceScores[alliance]);
    }

    public List<Alliance> CheckScore()
    {
        List<Alliance> winners = new List<Alliance>();
        foreach (KeyValuePair<Alliance, int> kvp in allianceScores)
        {
            if (kvp.Value >= GameManager.Instance.GetGameSettings().maxScore)
            {
                winners.Add(kvp.Key);
            }
        }
        return winners;
    }

    void ResetScore()
    {
        List<Alliance> keys = new List<Alliance>(allianceScores.Keys);
        foreach (Alliance key in keys)
        {
            allianceScores[key] = 0;
            GameEvent.ScoreChange(key, allianceScores[key]);
        }
    }

    public List<Alliance> GetWinners()
    {
        List<Alliance> winners = new List<Alliance>();
        int highScore = 0;

        foreach (KeyValuePair<Alliance, int> kvp in allianceScores)
        {
            if (kvp.Value >= highScore) { highScore = kvp.Value; }
        }
        foreach (KeyValuePair<Alliance, int> kvp in allianceScores)
        {
            if (kvp.Value == highScore) { winners.Add(kvp.Key); }
        }

        return winners;
    }
}
