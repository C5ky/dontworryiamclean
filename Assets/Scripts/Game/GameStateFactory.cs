﻿
public class GameStateFactory {

    public GameManager.GameState.Warmup Warmup { get; protected set; }
    public GameManager.GameState.Play Play { get; protected set; }
    public GameManager.GameState.End End { get; protected set; }

    public GameStateFactory(GameManager context)
    {
        Warmup = new GameManager.GameState.Warmup();
        Play = new GameManager.GameState.Play();
        End = new GameManager.GameState.End();

        Warmup.SetContextVariables(this, context);
        Play.SetContextVariables(this, context);
        End.SetContextVariables(this, context);
    }
}
