﻿public abstract class Item
{
    public abstract int Amount
    {
        get;
        set;
    }
}
