﻿public class PrayerItem : Item
{
	private int _amount;


	public PrayerItem(int amount)
	{
		_amount = amount;
	}


	public override int Amount { get; set; }


	public void AddAmount(int amount)
	{
		_amount += amount;
	}
}
