﻿public class PreacherItem : Item
{
    private int _amount;
    
    
    public PreacherItem(int amount)
    {
        _amount = amount;
    }
    

    public override int Amount
    {
        get;
        set;
    }

    
    public void AddAmount(int amount)
    {
        _amount += amount;
    }
}
