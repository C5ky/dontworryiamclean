﻿
public class PlayerEvent {

    public delegate void PlayerReadyAction(Alliance alliance, bool isReady);
    public delegate void PlayerManagerAction();

    public static event PlayerReadyAction OnPlayerReady;
    public static event PlayerManagerAction OnAllPlayersReady;
    public static event PlayerManagerAction OnCancelAllPlayersReady;

    public static void PlayerReady(Alliance alliance, bool isReady)
    {
        if (OnPlayerReady != null) { OnPlayerReady(alliance, isReady); }
    }
    public static void AllPlayersReady()
    {
        if (OnAllPlayersReady != null) { OnAllPlayersReady(); }
    }
    public static void CancelAllPlayersReady()
    {
        if (OnCancelAllPlayersReady != null) { OnCancelAllPlayersReady(); }
    }
}
