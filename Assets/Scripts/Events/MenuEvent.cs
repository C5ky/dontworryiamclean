﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuEvent : MonoBehaviour
{
    [SerializeField] private GameObject _playerSelection;
    [SerializeField] private GameObject _optionsPanel;
    [SerializeField] private GameObject _mainMenuPanel;

    [Header("Tutorial")]
    [SerializeField] private GameObject _tutCanvas;
    [SerializeField] private GameObject _tutPanel1;

    public void NewGame()
    {
        SceneManager.LoadScene(1);
        //_playerSelection.SetActive(true);
    }

    
    public void Tutorial()
    {
        _tutCanvas.SetActive(true);
        _tutPanel1.SetActive(true);
    }
    

    public void Options()
    {
        _mainMenuPanel.SetActive(false);   
        _optionsPanel.SetActive(true);
    }
    

    public void Quit()
    {
        //Debug.Log("Application quit.");
        Application.Quit();
    }
}
