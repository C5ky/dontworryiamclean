﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEvent {

    public delegate void AIAction(AI ai);

    public static event AIAction OnAIConverted;

    public static void AIConverted(AI ai)
    {
        if (OnAIConverted != null) { OnAIConverted(ai); }
    }
}
