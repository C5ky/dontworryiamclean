﻿
using System.Collections.Generic;

public class GameEvent {

    public delegate void GameAction(GameSettings settings);
    public delegate void GameEndAction(GameSettings settings, List<Alliance> winners);
    public delegate void GameScoreAction(Alliance alliance, int amount);
    public delegate void GameCountdownAction(int currentCountdown);

    public static event GameAction OnGameWarmup;
    public static event GameAction OnGameStart;
    public static event GameEndAction OnGameEnd;
    public static event GameScoreAction OnScoreChange;
    public static event GameCountdownAction OnWarmupCountdownChange;
    public static event GameCountdownAction OnPlayCountdownChange;
    public static event GameCountdownAction OnEndCountdownChange;

    public static void GameWarmup(GameSettings settings)
    {
        if (OnGameWarmup != null) { OnGameWarmup(settings); }
    }
    public static void GameStart(GameSettings settings)
    {
        if (OnGameStart != null) { OnGameStart(settings); }
    }
    public static void GameEnd(GameSettings settings, List<Alliance> winners)
    {
        if (OnGameEnd != null) { OnGameEnd(settings, winners); }
    }
    public static void ScoreChange(Alliance alliance, int value)
    {
        if (OnScoreChange != null) { OnScoreChange(alliance, value); }
    }
    public static void WarmupCountdownChange(int currentCountdown)
    {
        if (OnWarmupCountdownChange != null) { OnWarmupCountdownChange(currentCountdown); }
    }
    public static void PlayCountdownChange(int currentCountdown)
    {
        if (OnPlayCountdownChange != null) { OnPlayCountdownChange(currentCountdown); }
    }
    public static void EndCountdownChange(int currentCountdown)
    {
        if (OnEndCountdownChange != null) { OnEndCountdownChange(currentCountdown); }
    }
}
