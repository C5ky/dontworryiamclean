﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerSelectionEvent : MonoBehaviour
{
    public void Back()
    {
        gameObject.SetActive(false);
    }

    
    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
}
