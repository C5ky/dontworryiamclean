﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : Target, IAlliance {

    public Alliance alliance;

    public void Converted(Alliance alliance)
    {
        throw new System.NotImplementedException();
    }

    public Alliance GetAlliance()
    {
        return alliance;
    }

    public virtual void SetColor(Color newCol) { }

    public bool IsImmune()
    {
        return true;
    }
}
