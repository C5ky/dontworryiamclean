﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BuildingManager : MonoBehaviour {

    Dictionary<Alliance, List<Church>> allianceChurches = new Dictionary<Alliance, List<Church>>();

    private void Awake()
    {
        foreach (Church church in FindObjectsOfType<Church>())
        {
            if (allianceChurches.ContainsKey(church.alliance))
            {
                if (!allianceChurches[church.alliance].Contains(church)) { allianceChurches[church.alliance].Add(church); }
            }
            else
            {
                allianceChurches.Add(church.alliance, new List<Church>() { church });
            }
        }
    }

    public Church GetRandomEnemyChurch(Alliance alliance)
    {
        Church church = null;
        foreach (KeyValuePair<Alliance, List<Church>> kvp in allianceChurches.Where(v => v.Key != alliance))
        {
            if (kvp.Value != null && kvp.Value.Count > 0)
            {
                church = kvp.Value[Random.Range(0, kvp.Value.Count - 1)];
                break;
            }
        }
        return church;
    }
    public Church GetRandomAlliedChurch(List<Alliance> friendlyAlliences)
    {
        Church church = null;

        if (friendlyAlliences != null && friendlyAlliences.Count > 0)
        {
            List<Alliance> alliancesWithChurch = new List<Alliance>();
            foreach (Alliance alliance in friendlyAlliences)
            {
                if (alliance != Alliance.Neutral && allianceChurches.ContainsKey(alliance) && allianceChurches[alliance] != null)
                {
                    foreach (Church c in allianceChurches[alliance])
                    {
                        if (c != null && !alliancesWithChurch.Contains(alliance)) { alliancesWithChurch.Add(alliance); }
                    }
                }
            }

            if (alliancesWithChurch != null && alliancesWithChurch.Count > 0)
            {
                Alliance randomAlliance = alliancesWithChurch[Random.Range(0, alliancesWithChurch.Count - 1)];
                church = allianceChurches[randomAlliance][Random.Range(0, allianceChurches[randomAlliance].Count-1)];
            }
        }

        return church;
    }
}
