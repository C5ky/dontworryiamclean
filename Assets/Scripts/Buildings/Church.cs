﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Church : Building
{
    [SerializeField] List<MeshRenderer> colorComponents = new List<MeshRenderer>();
	[SerializeField] private int _health = 100;

    private void Awake()
    {
        Color newColor = alliance == Alliance.P1 ? Color.magenta : Color.cyan;
        SetColor(newColor);
    }

    public override void Interact(AI ai)
    {
        if (ai.GetAlliance() != alliance) { _health--; GameManager.Instance.scoreManager.AddScore(alliance, -1); }
        if (ai.GetAlliance() == alliance) { _health++; GameManager.Instance.scoreManager.AddScore(alliance, 1); }

        GameManager.Instance.aiManager.DestroyAI(ai);
        GameManager.Instance.aiManager.CreateAI(2);
    }

    public override void SetColor(Color newCol)
    {
        foreach (MeshRenderer mr in colorComponents)
        {
            if (mr != null)
            {
                Debug.Log("here");
                Material mat = mr.materials[1];
                Debug.Log(mat.name);
                mat.color = newCol;
            }
        }
    }
}
