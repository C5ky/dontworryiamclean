﻿using UnityEngine;

public interface IAlliance
{
    void Converted(Alliance alliance);
    void SetColor(Color newCol);
    bool IsImmune();
    Alliance GetAlliance();
    T GetComponent<T>();

}