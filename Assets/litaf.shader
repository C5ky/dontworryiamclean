Shader "LitAfWolololo" {
	Properties {
		_Color ("Main Color", Color) = (0.5,0.5,0.5,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_ShadowColor("Shadow Color", Color) = (0.5,0.5,0.5,1)
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
CGPROGRAM
#pragma surface surf ToonRamp

sampler2D _Ramp;
float4 _ShadowColor;

// custom lighting function that uses a texture ramp based
// on angle between light direction and normal
#pragma lighting ToonRamp exclude_path:prepass
inline half4 LightingToonRamp (SurfaceOutput s, half3 lightDir, half atten)
{
	#ifndef USING_DIRECTIONAL_LIGHT
	lightDir = normalize(lightDir);
	#endif
	
	half d = dot (s.Normal, lightDir);
	half3 ramp;

	if (d > .455) {
		ramp = d * .07 + (1- .07);
	} else {
		ramp = d * .6;
	}

	ramp = 1- (1-ramp) * (1-_ShadowColor);

	half4 c;
	c.rgb = s.Albedo * ramp;
	c.a = 0;
	return c;
}


sampler2D _MainTex;
float4 _Color;

struct Input {
	float2 uv_MainTex : TEXCOORD0;
};

void surf (Input IN, inout SurfaceOutput o) {
	o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb * _Color * _LightColor0.rgb;
}
ENDCG

	} 

	Fallback "Diffuse"
}
