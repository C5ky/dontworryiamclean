﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cycOffset : MonoBehaviour {

	[Range(0,1)]
	public float offss;
	public string statename;

	// Use this for initialization
	void Start () {
		Animator anim = GetComponent<Animator> ();
		anim.Play (statename, 0, Random.value * offss);
	}
}
